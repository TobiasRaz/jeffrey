export class Config {
    public static readonly endpoint : string = "https://api.stripe.com/v1";
    public static readonly uberEndpoint : string = "https://api.uber.com/v1.2";
}
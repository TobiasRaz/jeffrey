import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Race } from '../../models/race';

@Component({
  selector: 'calling-driver',
  templateUrl: 'calling-driver.html'
})
export class CallingDriverComponent implements OnChanges {

  @Input('run') run: Race;
  driver: any;

  constructor(private callnumber: CallNumber, private fire: FirebaseProvider) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initDriver()
  }

  ngAfterInit() {
  }

  initDriver() {
    if (this.run) {
      this.fire.getProfileChange(this.run.driver).subscribe((driver) => {
        if (driver) {
          let key: string = Object.keys(driver)[0]
          this.driver = driver[key]
        }
      })
    }
  }

  call(driver) {
  	this.callnumber.callNumber(driver.phoneNumber, true);
  }

  public supported(run: Race) {
    this.fire.setSupported(run)
  }

}

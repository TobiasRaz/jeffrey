import { Component, Input, Output, EventEmitter, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';
import { User } from '../../models/user';
import { Race } from '../../models/race';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../../providers/firebase/firebase';

@Component({
  selector: 'detail-chauffeur-bloc',
  templateUrl: 'detail-chauffeur-bloc.html'
})
export class DetailChauffeurBlocComponent implements OnChanges {
  
  @Output() driverLocation = new EventEmitter;
  @Input('drivers') drivers;
  @Input('user') user;
  @Input('race') race: Race;

  calling: boolean = false;
  driver: any;
  id: number = 0;
  last_id: number = 0;

  key: string;
  canceled: boolean = false;

  constructor(private cdr: ChangeDetectorRef, 
    private database: AngularFireDatabase,
    private fire: FirebaseProvider
    ) {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.displayDriver()
  }

  ngAfterViewInit() {
    this.displayDriver()
  }

  displayDriver() {
    if (this.drivers[this.id]) {
      this.driver = this.drivers[this.id]
      this.driverLocation.emit(this.driver)
      if (!this.driver.vehicle)
        this.driver.vehicle = {} 
      else {
        this.fire.getVehicle(this.driver.uid, this.driver.vehicle.id).then((vehicle) => {
          this.driver.vehicle = vehicle
        })
      }
      if (!this.driver.averageStars)
        this.driver.averageStars = 0
      this.cdr.detectChanges()
    } else {
      this.id = this.last_id
    }
  }

  next() {
    this.last_id = this.id;
    this.id = this.id + Number(1);
    this.displayDriver();
  }

  previous() {
    this.last_id = this.id;
    this.id = this.id - Number(1);
    this.displayDriver();
  }

  report(driver) {
    this.key = this.database.list(`call-list/${driver.uid}`).push({
      client: this.user.uid,
      origin_lat: this.race.origin.latitude,
      origin_lng: this.race.origin.longitude,
      destination_lat: this.race.destination.latitude,
      destination_lng: this.race.destination.longitude,
      origin_name: this.race.origin.placeName,
      destination_name: this.race.destination.placeName,
    }).key;
    this.calling = true;

    this.database.list(`call-list/${driver.uid}/${this.key}`).auditTrail().subscribe((responses) => {
      let stop = false;
      responses.forEach((response) => {
        if (response.type == 'child_removed') {
          stop = true;
        }
      })
      if (stop) {
        this.calling = false;
        if (!this.canceled)
          this.driver.occuped = true;
        else {
          this.canceled = false;
        }
      }
    });
  }

  cancel() {
    this.canceled = true
    this.database.list(`call-list/${this.driver.uid}`).remove(this.key);
    this.calling = false;
  }

  /*


  getDriverProfile() {
    console.log(this.vehicles)
    if (this.vehicles[this.id]) {
      this.fire.getProfile(this.vehicles[this.id].driver).then((_driver) => {
        for (var key in _driver) {
          this.driver = _driver[key]        
          this.driver.uid = this.vehicles[this.id].driver
          if (!this.driver.averageStars)
            this.driver.averageStars = 0
        }
        this.driverLocation.emit(this.vehicles[this.id]);
      })
    } else {
      this.id = this.last_id;
    }
  }

  report() {
    this.key = this.database.list(`call-list/${this.driver.uid}`).push({
      client: this.user.uid,
      origin_lat: this.race.origin_lat,
      origin_lng: this.race.origin_lng,
      destination_lat: this.race.destination_lat,
      destination_lng: this.race.destination_lng,
      origin_name: this.race.origin_name,
      destination_name: this.race.destination_name,
    }).key;
    this.calling = true;

    this.database.list(`call-list/${this.driver.uid}/${this.key}`).auditTrail().subscribe((responses) => {
      let stop = false;
      responses.forEach((response) => {
        if (response.type == 'child_removed') {
          stop = true;
        }
      })
      if (stop) {
        this.calling = false;
        if (!this.canceled)
          this.driver.occuped = true;
        else {
          this.canceled = false;
        }
      }
    });
  }*/

}

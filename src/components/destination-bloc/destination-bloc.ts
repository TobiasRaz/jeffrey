import { Component, Input } from '@angular/core';
import { Race } from '../../models/race';

@Component({
  selector: 'destination-bloc',
  templateUrl: 'destination-bloc.html'
})
export class DestinationBlocComponent {

  @Input('race') race = {
  	origin: {
  	  placeName: ""
  	},
  	destination: {
  	  placeName: ""
  	},
  } as Race;

}

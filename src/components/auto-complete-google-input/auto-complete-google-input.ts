import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';

declare var google;
@Component({
  selector: 'auto-complete-google-input',
  templateUrl: 'auto-complete-google-input.html'
})
export class AutoCompleteGoogleInputComponent {

  @Output() position = new EventEmitter;
  @Input('property') property: {
    id: string,
    placeholder: string,
    value?: string
  } = {
    id: 'input',
    placeholder: 'Adresse'
  };
  
  constructor(private platform: Platform, private alertCtrl: AlertController) {
  }

  prepareAutocomplete() {
  	this.verifyGoogle().then(() => {
  	  let input = <HTMLInputElement>document.querySelector(`#${this.property.id} input`);
  	  let autocomplete: any = {};
      let options = { componentRestrictions: { country: "fr" }};
      //delete options.componentRestrictions;
      autocomplete = new google.maps.places.Autocomplete(input, options);
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        let place = autocomplete.getPlace();
        this.position.emit(place)
        this.property.value = place.name
      })
  	});
  }

  ngAfterViewInit() {
  	this.prepareAutocomplete();
  }

  private verifyGoogle() {
    return new Promise((resolve) => {
      if (typeof(google) == 'undefined') {
        let alert: {} = {
          title : 'Oh!',
          subTitle: 'L\'API de Google Map n\'est pas fonctionnel',
          message: 'Veuillez vérifier votre connexion avant de réessayer',
          buttons: [
            {
              text: 'Réessayer',
              handler: () => {
                this.verifyGoogle();
              }
            },
            {
              text: 'Quitter',
              handler: () => {
                this.platform.exitApp();
              }
            },
          ],
          enableBackdropDismiss: false,
        }

       this.showAlert(alert);
       resolve(false);
      } else {
        resolve(true);
      }
    });
  }

  private showAlert(alert) {
    this.alertCtrl.create(alert).present();
  }

}

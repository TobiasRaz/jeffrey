import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Race } from '../../models/race';

@Component({
  selector: 'client-detail-bloc',
  templateUrl: 'client-detail-bloc.html'
})
export class ClientDetailBlocComponent implements OnChanges {

  @Input('run') run: Race;
  client: any = {
  	profile: {}
  };
  
  constructor(private fire: FirebaseProvider) {
  }

  ngOnChanges(changes: SimpleChanges) {
  	if (changes.run)
      this.presentClient()
  }

  private presentClient() {
  	if (this.run.client) {
  	  this.fire.getProfile(this.run.client).then((profile) => {
  	  	if (profile) {
	  	  let key = Object.keys(profile)[0]
        this.client.uid = this.run.client
	  	  this.client.profile = profile[key]
  	  	}
  	  })
  	}
  }

  public stop(run: Race) {
    this.fire.removeRunning(run.client)
    this.fire.removeDriving(run.driver)
    this.fire.createArchive(run)
  }

}

import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular' ;
/**
 * Generated class for the LoadingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'loading',
  templateUrl: 'loading.html'
})
export class LoadingComponent {

  @Input('search') search;

  constructor(private events:Events) {
  }

  cancelSearch() {
  	this.events.publish('close:loading');
  }
}

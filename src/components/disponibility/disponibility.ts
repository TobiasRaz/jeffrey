import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'disponibility',
  templateUrl: 'disponibility.html'
})
export class DisponibilityComponent {

  @Output() diponibility = new EventEmitter

  constructor() {
  }

  ok() {
  	this.diponibility.emit(true);
  }

  no() {
  	this.diponibility.emit(false);
  }

}

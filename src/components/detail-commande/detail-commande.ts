import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Race } from '../../models/race';
import { UberProvider } from '../../providers/uber/uber';

@Component({
  selector: 'detail-commande',
  templateUrl: 'detail-commande.html'
})
export class DetailCommandeComponent implements OnChanges {
  @Input('run') run = {} as Race;
  @Input('service') service: any;
  @Output() order = new EventEmitter()

  constructor(private uber: UberProvider) {

  }

  ngOnChanges(changes: SimpleChanges) {
  	this.uber.getEstimation(this.run).then((prices) => {
  	  let i: number;
  	  switch (this.service.title) {
  	  	case "VTC":
  	  		i = 1
  	  		break;
  	  	case "VAN":
  	  		i = 4
  	  		break;
  	  }
  	  this.service.price = prices[i].estimate;
  	  this.service.duration = Math.round(prices[i].duration/60) + 'mn';
  	})
  }

}

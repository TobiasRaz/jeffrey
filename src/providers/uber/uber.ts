import { Injectable } from '@angular/core';
import { WsProvider } from '../ws/ws';
import { Race } from '../../models/race';
import { Config } from '../../config/config';
import { Loading } from 'ionic-angular';

@Injectable()
export class UberProvider extends WsProvider {

  protected endpointUrl: string = Config.uberEndpoint;
  private token: string = "DyrzgIixy84HOUcGLwf9xwnKw0LZ7E_bvTkrP9fe";
  public loading: Loading;

  getEstimation(run: Race, seat_count: number = 2) {
    return new Promise((resolve) => {
      let param: {
        start_latitude: number,
        start_longitude: number,
        end_latitude: number,
        end_longitude: number,
      } = {
        start_latitude: run.origin.latitude,
        start_longitude: run.origin.longitude,
        end_latitude: run.destination.latitude,
        end_longitude: run.destination.longitude,
      };
      let uberRef$ = this.fetch('estimates/price', 'GET', param, {'Authorization': 'Token ' + this.token })
      this.loading.dismiss()

      uberRef$
      .subscribe((response: any) => {
        let prices = response.json().prices;
        resolve(prices)
      })
    })
  }
}

import { Injectable } from '@angular/core';
import { LoadingController, Loading, Events } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { storage } from 'firebase';
import { Profile } from '../../models/profile';
import { User } from '../../models/user';
import { Race } from '../../models/race';
import { Position } from '../../models/position';
import { Vehicle } from '../../models/vehicle';
import { SuperProvider } from '../super/super';


@Injectable()
export class FirebaseProvider extends SuperProvider {

  loading: Loading;

  constructor(private afAuth: AngularFireAuth, public loadingCtrl: LoadingController, 
    private database: AngularFireDatabase,
    private events: Events
    ) {
    super(loadingCtrl)
  }

  sendResetPass(email: string) {
  	return new Promise((resolve, reject) => {
      this.presentLoading();
  	  this.afAuth.auth.sendPasswordResetEmail(email).then(() => {
        this.loading.dismiss();
  	  	resolve();
  	  }).catch((e) => {
        this.loading.dismiss();
        reject(e);
      })
  	})
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  register(credential: User, profile: Profile) {
    return new Promise((resolve, reject) => {
      this.presentLoading();
      this.afAuth.auth.createUserWithEmailAndPassword(credential.email, credential.password)
      .then((user) => {
        this.database.list(`profile/${user.uid}`).push(profile)
        .then((create) => {
          resolve(true);
        })
      })
      .catch((e) => {
        this.loading.dismiss();
        reject(e);
      });
    })
  }

  login(credential: User) {
    return new Promise((resolve, reject) => {
      this.presentLoading();
      this.afAuth.auth.signInWithEmailAndPassword(credential.email, credential.password)
      .then((user) => {
        this.loading.dismiss();
        resolve(user);
      })
      .catch((e) => {
        this.loading.dismiss();
        reject(e);
      })
    })
  }

  loggedin() {
    return new Promise((resolve, reject) => {
      this.afAuth.authState.take(1).subscribe((user) => {
        resolve(user)
      })
    })
  }

  loggedinChange() {
    return this.afAuth.authState;
  }

  logout() {
    return new Promise((resolve) => {
      this.presentLoading();
      this.afAuth.auth.signOut().then(() => {
        this.loading.dismiss();
        this.events.publish('user:logout');
        resolve(true);
      })
    })
  }

  getProfile(uid) {
    return new Promise((resolve) => {
      this.database.object(`profile/${uid}`).valueChanges().subscribe((profile) => {
        resolve(profile)
      })
    })
  }

  getProfileChange(uid) {
    return this.database.object(`profile/${uid}`).valueChanges();
  }

  updateProfile(user: User, profile: Profile) {
    return new Promise((resolve) => {
      let item = profile.item;
      this.database.list(`profile/${user.uid}`).update(item, profile).then(() => {
        resolve(true);
      });
    })
  }

  storeImageProfile(image, user: User) {
    return new Promise((resolve) => {
      this.presentLoading();

      let pictures = storage().ref(`pictures/profile/${user.uid}`);
      let ref = pictures.putString(image, 'data_url');
      ref.then(() => {
        this.loading.dismiss();
        resolve(ref.snapshot.downloadURL)
      }).catch((e) => {
        alert(JSON.stringify(e))
      })
    })
  }

  storeImageVehicle(image, user: User) {
    return new Promise((resolve) => {
      this.presentLoading();

      let pictures = storage().ref(`pictures/vehicle/${user.uid}`);
      let ref = pictures.putString(image, 'data_url');
      ref.then(() => {
        this.loading.dismiss();
        resolve(ref.snapshot.downloadURL)
      }).catch((e) => {
        alert(JSON.stringify(e))
      })
    })
  }

  getImageUrl() {
    return new Promise((resolve) => {
      this.presentLoading(); 
    })
  }

  updateVehicle(user: User, vehicle: Vehicle) {
    return new Promise((resolve) => {
      this.presentLoading();
      this.database.object(`vehicles-list/${user.uid}/${vehicle.key}`).update(vehicle).then(() => {
        this.loading.dismiss();
        resolve(true);
      })
    })
  }

  createVehicle(user: User, vehicle: Vehicle) {
    return new Promise((resolve) => {
      this.presentLoading();
      let vehiclesItemRef$ = this.database.list(`vehicles-list/${user.uid}`);
      let _vehicle: Vehicle = {
        brand: vehicle.brand,
        placeNb: vehicle.placeNb,
        type: vehicle.type,
        driver: user.uid,
      }
      if (vehicle.picture_url)
        _vehicle.picture_url = vehicle.picture_url
      vehiclesItemRef$.push(_vehicle).then(() => {
        this.loading.dismiss();
        resolve(true)
      })
    })
  }

  saveVehile(user: User, vehicle: Vehicle) {
    if (vehicle.key) {
      return this.updateVehicle(user, vehicle);
    } else {
      return this.createVehicle(user, vehicle)
    }
  }

  getVehicles(user: User) {
    return this.database.object(`vehicles-list/${user.uid}`).valueChanges()
  }

  getVehicle(driver_uid: string, vehicle_id: string): Promise<{}> {
    return new Promise((resolve) => {
      this.database.object(`vehicles-list/${driver_uid}/${vehicle_id}`).valueChanges()
      .subscribe((vehicle) => {
        resolve(vehicle)
      })
    });
  }

  setDispo(driver: User, vehicle: Vehicle) {
    return new Promise((resolve) => {
      this.presentLoading();
      let param = {
        vehicle: vehicle.key
      };
      let ref = this.database.list(`vehicles-dispo/${vehicle.type}/${driver.uid}`).push(param);
      let key = ref.key;
      ref.then(() => {
        this.loading.dismiss();
        resolve(key)
      })
    })
  }

  getDispo(service: any) {
    return new Promise((resolve) => {
      let _vehicles = [];
      let _v: any;
      this.database.object(`vehicles-dispo/${service.title}`).valueChanges().subscribe((vehicles) => {
        for (var key in vehicles) {
          for (var i in vehicles[key]) {
            _v = vehicles[key][i];
            _v.driver = key
          }
          _vehicles.push(_v);
        }
        resolve(_vehicles);
      })
    })
  }

  getAllDispo() {
    return this.database.object(`vehicles-dispo/`).valueChanges();
  }

  getDispoChange(category: string) {
    return this.database.object(`vehicles-dispo/${category}`).valueChanges();
  }

  public removeDispo(driver_uid: string, vehicle: Vehicle) {
    return new Promise((resolve) => {
      this.presentLoading();
      this.database.list(`vehicles-dispo/${vehicle.type}`).remove(driver_uid).then(() => {
        this.loading.dismiss();
        resolve(true);
      })
    })
  }

  removeVehicule(user: User, vehicle: Vehicle) {
    return new Promise((resolve) => {
      this.presentLoading();
      this.database.list(`vehicles-list/${user.uid}`).remove(vehicle.key).then(() => {
        this.loading.dismiss();
        resolve(true);
      })
    })
  }

  public removeCallList(run: Race) {
    return new Promise((resolve) => {
      this.presentLoading();
      this.database.list(`call-list/${run.driver}`).remove(run.key)
      .then(() => {
        this.loading.dismiss();
        resolve(true);
      })
    })
  }

  private addRunning(run: Race, vehicle: Vehicle) {
    return new Promise((resolve) => {
      this.removeCallList(run).then(() => {
        run.type = vehicle.type;
        let ref$ = this.database.list(`running/${run.client}/${run.driver}`).push(run)
        ref$.then(() => {
          this.removeDispo(run.driver, vehicle).then(() => {
            this.database.list(`running/${run.client}/${run.driver}`).update(ref$.key, { key: ref$.key })
            .then(() => resolve(true))
          })
        })
      })
    })
  }

  removeRunning(client_uid: string) {
    return new Promise((resolve) => {
      this.database.list(`running/${client_uid}/`).remove()
      .then(() => {
        resolve(true)
      })
    })
  }

  removeDriving(driver_uid: string) {
    return new Promise((resolve) => {
      this.database.list(`driving/${driver_uid}/`).remove()
      .then(() => {
        resolve(true)
      })
    })
  }

  private addDriving(run: Race) {
    return new Promise((resolve) => {
      let state = {
        status: "in progress"
      }
      let ref$ = this.database.list(`driving/${run.driver}/${run.client}`).push(state)
      ref$.then(() => resolve(ref$.key));
    })
  }

  createRuning(run: Race, vehicle: Vehicle) {
    this.addDriving(run).then((key: string) => {
      run.driving = key
      this.addRunning(run, vehicle).then(() => {
      })
    })
  }

  getRunning(user: User) {
    return this.database.object(`running/${user.uid}`).valueChanges()
  }

  getDriving(user: User) {
    return this.database.object(`driving/${user.uid}`).valueChanges();
  }

  setSupported(run: Race) {
    this.database.list(`driving/${run.driver}/${run.client}`)
    .update(run.driving , {
      status: 'supported'
    })
    this.database.list(`running/${run.client}/${run.driver}`)
    .update(run.key, {
      status: 'supported'
    })
  }

  createArchive(run: Race) {
    run.status = "finish"
    let ref$ = this.database.list(`archive/${run.client}`).push(run)
  }

  getArchive(client_uid: string) {
    return this.database.list(`archive/${client_uid}`).valueChanges()
  }

  getPaid(client_uid: string) {
    return this.database.list(`paid/${client_uid}`).valueChanges()
  }

  setPaid(user_uid: string, price: number, token: string) {
    this.getArchive(user_uid).take(1).subscribe((unpaid: any) => {
      if (unpaid && unpaid[0]) {
        this.database.list(`archive`).remove(user_uid)
        unpaid[0].status = "Payé"
        unpaid[0].price = price
        unpaid[0].token = token
        this.database.list(`paid/${user_uid}`).push(unpaid[0])
      }
    })
  }

}

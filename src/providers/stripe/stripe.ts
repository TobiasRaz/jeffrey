import { Injectable } from '@angular/core';
import { Http, Response, Headers , RequestOptions } from '@angular/http';
import { LoadingController } from 'ionic-angular';
import { Stripe } from '@ionic-native/stripe';
import { WsProvider } from '../ws/ws' ;
import { StripeService, Elements, Element as StripeElement, ElementOptions } from 'ngx-stripe' ;
import { Storage } from '@ionic/storage'

@Injectable()
export class StripeProvider extends WsProvider {

  card: {};
  tokenId: any;

  constructor(public http: Http, 
    private stripe: Stripe, 
    private stripeService: StripeService,
    public loadingCtrl: LoadingController,
    private storage: Storage
    ) {
    super(http,loadingCtrl);
  }

  public validateCardNumber(cardNumber: string) {
  	let validate = this.stripe.validateCardNumber(cardNumber)
    return validate;
  }

  public validateCVC(cvc: string) {
  	let validate = this.stripe.validateCVC(cvc)
    return validate;
  }

  public validateExpiryDate(expMonth: string, expYear) {
  	let validate = this.stripe.validateExpiryDate(expMonth, expYear)
    return validate;
  }
  // public loadStripe() {
  //   this.stripe.setPublishableKey('pk_test_ymJmF756QPBJwiTuR0iPel7k');
  //   let card = {
  //     number: '4242424242424242',
  //     expMonth: 12,
  //     expYear: 2020,
  //     cvc: '220'
  //   };

  //   this.stripe.createCardToken(card)
  //     .then((token) => {
  //        let payment = {
  //          amount:400,
  //          currency:'eur',
  //          source: token.id,
  //          description:"charge pour"
  //        }         
  //        return new Promise ((resolve)=> {
  //          this.fetch('charges/' + token.id,'POST', payment).subscribe((response) => {
  //            let query = response.json() ;
  //            let success = query.success ;
  //            if(success)
  //              resolve(true) ;
  //            else
  //              resolve(false) ;
  //          })

  //        })
  //        // console.log(token.id)
  //     })
  //     .catch((error) => {
  //         console.error(error)
  //     });
  // }

  StripeCharge(tokenId, price: number) {
    return new Promise((resolve, reject) => {
      console.log(tokenId);
      let body = 'card=' + tokenId + `&currency=eur&amount=${price * 100}`; 
      console.log(body);
       //let bodyString = JSON.stringify(body);
       let headers = new Headers(
         {
          'Content-Type' : 'application/x-www-form-urlencoded',
          //'Authorization'  :  'Bearer sk_test_WX63K9MrFdxUiR5HZthlcC8a'
          'Authorization'  :  'Bearer sk_test_98WwqxiHAQK7dmBj6XKXeJbZ'
         }) ;

       let options = new RequestOptions({ headers: headers });

       let loading = this.loadingCtrl.create({
         enableBackdropDismiss: false,
         dismissOnPageChange: false
       })
       loading.present()
       this.http.post(this.endpointUrl + "/charges", body , options)
       .subscribe(response =>{
         loading.dismiss()
         resolve(response.json());
       },error => {
         loading.dismiss()
         reject(error.json());
       });
    })
  }

  generateToken(card,name_): Promise<{}> {
    let cards =  {
      card:card,
      name:name_
    }
    const name = cards.name;
    return new Promise((resolve, reject) => {
      this.stripeService
      .createToken(cards.card, { name })
      .subscribe((result) => {
        if (result)
          resolve(result)
      })
    })
  }

}

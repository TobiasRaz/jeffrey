import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Platform ,AlertController, AlertOptions, Alert } from 'ionic-angular';

@Injectable()
export class NetworkProvider {

  alert: Alert;

  constructor(private network: Network, private alertCtrl: AlertController, private platform: Platform,
    private diagnostic: Diagnostic) {}

  public checkConnection() {
    this.network.onDisconnect().subscribe(() => {
  	let alertOption: AlertOptions = {
        title: 'Erreur de connection',
  	    message: 'Verifier votre connection avant de continuer',
  	    enableBackdropDismiss: false,
  	    buttons: [{
  	  	  text: 'Paramètre',
          handler: () => this.diagnostic.switchToWirelessSettings()
  	    },
  	    {
  	  	  text: 'Quitter',
  	  	  handler: () => this.platform.exitApp()
  	    }]
  	  }
  	  this.alert = this.alertCtrl.create(alertOption);
      this.alert.present();
    });
    this.network.onConnect().subscribe(() => {
      this.alert.dismiss();
    })
  }

}

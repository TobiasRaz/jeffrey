import { Http, RequestOptions, Headers, Request  } from '@angular/http';
import { Injectable } from '@angular/core';
import { Config } from '../../config/config';
import { LoadingController, Loading } from 'ionic-angular';
import { SuperProvider } from '../super/super';

@Injectable()
export class WsProvider extends SuperProvider {
 
  protected endpointUrl: string = Config.endpoint;
  public loading: Loading;
 
  constructor(public http: Http,public loadingCtrl: LoadingController) {
 	super(loadingCtrl);
  }
 
  public fetch(resource: string = "",method: string = "GET",params: {[key:string]: any}={},headers:{[key: string]: string} = {})
  {
    this.presentLoading();
    let _headers: Headers = new Headers();
    let _data: any;
    let url = this.endpointUrl + '/' + resource;
      
    if(Object.keys(headers).length > 0)
    {
      for(let key in headers){
        _headers.append(key, headers[key]);
      }
    } 

    url += '?'
    
    switch(method)
    {
      case('GET'):
        for(let key in params){
          url += key +'=' + params[key] + '&';
        }
        _data = '';
      break;
    }

    let _requestOptions: RequestOptions = new RequestOptions({
      url: url,
      method: method,
      headers: _headers,
      body: _data,
    });
    
    return this.http.request( new Request( _requestOptions ) );
  }
 
}
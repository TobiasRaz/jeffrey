import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';

@Injectable()
export class SuperProvider {

  public loading: Loading;

  constructor(public loadingCtrl: LoadingController) {}

  presentLoading() {
  	if (!this.loading) {
	    this.loading = this.loadingCtrl.create({
	      dismissOnPageChange: true,
	      enableBackdropDismiss: false
	    });
	    this.loading.present();
  	}
  }

}

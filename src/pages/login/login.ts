import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, LoadingController,
	MenuController, Events } from 'ionic-angular';
import { User } from '../../models/user';
import { Profile } from '../../models/profile';
import { FormPage } from '../../classes/form-page';
import { RegisterPage } from '../register/register';
import { ForgoutPage } from '../forgout/forgout';
import { FirebaseProvider } from '../../providers/firebase/firebase';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage extends FormPage {

  user = {} as User;
  profile = {} as Profile;
  protected form : FormGroup;

  constructor(public navCtrl: NavController, private fire: FirebaseProvider,
              public navParams: NavParams,
              public formBuilder : FormBuilder, private menuCtrl: MenuController,
              public toastCtrl: ToastController,
              private loadingCtrl: LoadingController, private events: Events
              ) {
    super(toastCtrl);
    this.menuCtrl.enable(false);
    this.form = formBuilder.group({
        login: ['', Validators.compose([Validators.pattern(/^([\w\-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})$/), Validators.required])],
        hash: ['',Validators.required]
    });
  }

  ionViewCanEnter() {
    this.events.publish('user:registered', false);
  }

  public login(user: User) {
    if (this.form.valid) {
      let value = this.form.value;
      this.user = {
        email: value.login,
        password: value.hash
      }
      this.fire.login(this.user).then((user: any) => {

        this.events.publish('select:user', user);
        this.fire.getProfile(user.uid).then((profile) => {
          if (profile) {
            for (var key in profile) {
              this.profile = profile[key]
              this.profile.item = key
            }
          }
        })
      })
      .catch((e) => {
        switch (e.code) {
          case "auth/user-not-found":
            this.presentError("Il n'y a pas d'enregistrement utilisateur correspondant à cet identifiant. L'utilisateur a peut-être été supprimé")            
            break;
          case "auth/wrong-password":
            this.presentError("Le mot de passe est invalide ou l'utilisateur n'a pas de mot de passe.")
            break;
        }
      })
    } else {
      this.reportError();
    }
  }

  public getMessageError(key: string, validator: string) {
    let message_errors = [];
    message_errors['login'] = [];
    message_errors['hash'] = [];
    message_errors['login']['required'] = 'Adresse e-mail requis!';
    message_errors['login']['pattern'] = 'Adresse e-mail invalid';
    message_errors['hash']['required'] = 'Mot de passe requis!';
    return message_errors[key][validator];
  }

  register() {
    this.navCtrl.push(RegisterPage).then(() => this.events.publish('user:registered', true))
  }
  
  forgetPassword(){
    this.navCtrl.push(ForgoutPage)
  }

}

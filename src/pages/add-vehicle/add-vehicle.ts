import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController, ToastController } from 'ionic-angular';
import { Vehicle } from '../../models/vehicle';
import { User } from '../../models/user';

import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Camera, CameraOptions } from '@ionic-native/camera';
@IonicPage()
@Component({
  selector: 'page-add-vehicle',
  templateUrl: 'add-vehicle.html',
})
export class AddVehiclePage {

  vehicle = {} as Vehicle;
  user = {} as User;
  vehiclesItemRef$: any;

  constructor(private fire: FirebaseProvider, private navParams: NavParams, private navCtrl: NavController,
    private toastCtrl: ToastController , private camera: Camera) {
    this.user = this.navParams.get('user');
    if (this.navParams.get('vehicle'))
  	  this.vehicle = this.navParams.get('vehicle');
  }

  ionViewDidLoad() {}

  add(vehicle: Vehicle) {
    if (vehicle.brand && vehicle.type && vehicle.placeNb) {
  	  this.fire.saveVehile(this.user, vehicle).then(() => {
  	  	this.navCtrl.pop()
  	  })
  	} else {
      this.presentError('Vous devez compléter tous les champs')
    }
  }

  public presentError(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
    });

    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }

  takePictureVehicle() {
    const options: CameraOptions = {
      quality: 50,
      targetHeight: 600,
      targetWidth: 600,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    
    this.camera.getPicture(options).then((imageData) => {
      let image = `data:image/jpeg;base64,${imageData}`;
      this.fire.storeImageVehicle(image, this.user).then((url: string) => {
        this.vehicle.picture_url = url;
      }).catch((e) => {
        alert(JSON.stringify(e))
      })
    })
  }

}

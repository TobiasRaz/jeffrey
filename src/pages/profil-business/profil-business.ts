import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,
  MenuController, Loading } from 'ionic-angular';
  import { User } from '../../models/user';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ProfilBusinessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profil-business',
  templateUrl: 'profil-business.html',
})
export class ProfilBusinessPage {

  business: any = {};
  user = {} as User;
  loading: Loading;
  constructor(
  	private menuCtrl: MenuController,
    public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage, private toastCtrl: ToastController) {
  	 this.user = this.navParams.get('user');
    this.menuCtrl.enable(true);
  }

  ionViewDidLoad() {
  	this.storage.get('BUSSINESS:profil').then((bussin) => {
      if (bussin) {
        this.business = JSON.parse(bussin);
      }
    })
  }
  save(business) {
    this.storage.set('BUSSINESS:profil', JSON.stringify(business)).then(() => {
      this.presentError('Enregistré');
       this.navCtrl.pop();
    })
  }
  public presentError(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
    });

    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, MenuController, LoadingController, Loading } from 'ionic-angular';

import { YourRacePage } from '../your-race/your-race';
import { MapPage } from '../map/map';

        /* using cordova */
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';

import { User } from '../../models/user';
import { Profile } from '../../models/profile';
import { Race } from '../../models/race';
import { Position } from '../../models/position';

@IonicPage()
@Component({
  selector: 'page-option',
  templateUrl: 'option.html',
})
export class OptionPage {

  profile = {} as Profile;
  user = {} as User;
  loading: Loading;

        /* using cordova */
  constructor(private platform : Platform, private diagnostic : Diagnostic,
    private navCtrl: NavController,
    private navParams: NavParams,
    private menu: MenuController,
    private geolocation: Geolocation,
    private loadingCtrl: LoadingController
    ) {
    this.menu.enable(true);
    this.user = this.navParams.get('user')
    this.profile = this.navParams.get('profile')
  }

  ionViewDidLoad() {
        /* using cordova */
    this.presentLoading();
    this.diagnostic.getLocationAuthorizationStatus().then((authorized) => {
      if (authorized == this.diagnostic.permissionStatus.GRANTED) {
        this.diagnostic.isLocationEnabled().then((enable) => {
          if (enable) {
            this.displayMap();
          } else {
            this.loading.dismiss();
          }
        })
      }
    })
    .catch(() => this.loading.dismiss())
  }

  displayMap() {
    this.geolocation.getCurrentPosition().then((position) => {
      this.loading.dismiss()
      let race = {
        origin: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          placeName: 'Votre position actuelle'
        },
        destination: {} as Position
      } as Race;
      this.navCtrl.setRoot(MapPage, { user: this.user, profile: this.profile, run: race })
    })
  }

  enableLocation() {
        /* using cordova */
    this.diagnostic.requestLocationAuthorization(this.diagnostic.locationAuthorizationMode.ALWAYS).then((authorized) => {
      switch (authorized) {
        case this.diagnostic.permissionStatus.DENIED:
        case this.diagnostic.permissionStatus.DENIED_ALWAYS:
        case this.diagnostic.permissionStatus.NOT_REQUESTED:
          this.diagnostic.switchToSettings();
          break;
        case this.diagnostic.permissionStatus.GRANTED:
          this.diagnostic.isLocationEnabled().then((enable) => {
            if (!enable)
              this.diagnostic.switchToLocationSettings();
            else
              this.displayMap();
          })
          break;
      }
    }).catch((error) => {
      this.platform.exitApp();
    })
  }

  enter() {
    this.navCtrl.push(MapPage, { user: this.user, profile: this.profile, 
      complete_input: {
        placeholder: 'Lieu de prise en charge'
      } 
     })
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      enableBackdropDismiss: false,
      dismissOnPageChange: true
    })
    this.loading.present();
  }

}

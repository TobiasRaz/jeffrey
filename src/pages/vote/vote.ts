import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController, ModalController } from 'ionic-angular';
import { User } from '../../models/user';
import { Race } from '../../models/race';
import { Profile } from '../../models/profile';
import { FirebaseProvider } from '../../providers/firebase/firebase'
import { StripePage } from '../../pages/stripe/stripe';

@IonicPage()
@Component({
  selector: 'page-vote',
  templateUrl: 'vote.html',
})
export class VotePage {
  user: User;
  run: Race ;
  price: number;
  profileDriver = {} as Profile;
  paid: boolean = false;
  checks: [string, string, string, string, string] = ["uncheck","uncheck","uncheck","uncheck","uncheck"]
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private firebase: FirebaseProvider,
    private modal: ModalController,
    public viewCtrl : ViewController) {
    this.user = navParams.get('user') ;
    this.run = navParams.get('run') ;
  	this.price = navParams.get('price') ;
    this.initProfileDriver()
  }

  private initProfileDriver() {
    if (this.run) {
      this.firebase.getProfile(this.run.driver).then((profile) => {
        let key = Object.keys(profile)[0]
        this.profileDriver = profile[key]
      })
    }
  }

  private checkout() {
    let _run = this.run
    let stripeModal = this.modal.create(StripePage, { user: this.user, price: this.price, run: _run }, {
      enableBackdropDismiss: false
    })
    stripeModal.present()
    stripeModal.onDidDismiss((data) => {
      if (data.success) {
        this.run = data.run
        this.firebase.setPaid(this.user.uid, this.price, data.token)
        this.paid = true
      }
    })
  }

  public check(start: number) {
    if (start==0 && this.checks[0] == "check" && this.checks[1] != "check")
      this.checks[0] = "uncheck"
    else {
      for (var i = this.checks.length - 1; i >= 0; i--) {
        if (i<=start) 
          this.checks[i] = "check"
        else
          this.checks[i] = "uncheck"
      }
    }
  }

}

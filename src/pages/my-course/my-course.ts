import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ErrorPage } from '../error/error';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { User } from '../../models/user';
import { Race } from '../../models/race';
import { RacingDetailPage } from '../racing-detail/racing-detail';


@IonicPage()
@Component({
  selector: 'page-my-course',
  templateUrl: 'my-course.html',
})
export class MyCoursePage {
  user = {} as User;
  course = {} as Race;
  courses: [{}];

  constructor(public navCtrl: NavController,
  public navParams: NavParams,
  public FirProvider: FirebaseProvider
  ) 
  {
  this.user = this.navParams.get('user');  
  this.FirProvider.getRunning(this.user).subscribe((runs) => {
      if (runs) {
        let driver_uid: string = Object.keys(runs)[0];
        let key: string = Object.keys(runs[driver_uid])[0];
        let value  = runs[driver_uid][key] ;
        let date = new Date() ;
        let D = date.getDate().toString();
        let M = (date.getMonth() + 1).toString();
        if (date.getMonth() +1 < 10)
          M = '0' + M
        let Y = date.getFullYear().toString();
        this.course = {
          destination : value.destination,
          origin : value.origin,
          date : D + '/ ' + M + ' / ' + Y,
          driver: driver_uid
        }
        console.log(this.course);
      }
    })
  this.preparArchive();
  }

  preparArchive() {
    this.FirProvider.getPaid(this.user.uid).subscribe((archives: [{}]) => {
      this.courses = archives
    })
  }

  signaler() {
    this.navCtrl.push(ErrorPage) ;
  }

  detail(course) {
    this.navCtrl.push(RacingDetailPage,{course : course});
  }

}

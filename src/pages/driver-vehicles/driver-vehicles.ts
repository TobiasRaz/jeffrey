import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ActionSheetController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';

import { AddVehiclePage } from '../add-vehicle/add-vehicle';
import { WaitingListPage } from '../waiting-list/waiting-list';

import { Vehicle } from '../../models/vehicle';
import { User } from '../../models/user';
import { Profile } from '../../models/profile';
import { Position } from '../../models/position';

import { FirebaseProvider } from '../../providers/firebase/firebase';

@IonicPage()
@Component({
  selector: 'page-driver-vehicles',
  templateUrl: 'driver-vehicles.html',
})
export class DriverVehiclesPage {

  vehiclesListRef$: any;
  vehicles = [];
  position = {} as Position;
  user = {} as User;
  profile = {} as Profile;

  constructor(
  	public navCtrl: NavController, public navParams: NavParams,
    private fire: FirebaseProvider, private geolocation: Geolocation,
    public action: ActionSheetController,
    private loading: LoadingController, private storage: Storage
  	) {
    this.user = this.navParams.get('user');
    this.profile = this.navParams.get('profile');
    this.fire.getVehicles(this.user).subscribe((vehicles) => {
      this.vehicles = []
      for (var key in vehicles) {
        vehicles[key].key = key;
        this.vehicles.push(vehicles[key]);
      }
    })
  }

  ionViewCanEnter() {
    let vehicle = {} as Vehicle;
    this.storage.get('VEHICLE_TYPE').then((type) => {
      if (type) {
        vehicle.type = type;
        this.fire.removeDispo(this.user.uid, vehicle).then(() => {
          this.storage.remove('VEHICLE_TYPE')
        })
      }
    })
  }
  
  addVehicle() {
    this.navCtrl.push(AddVehiclePage, { user: this.user });
  }

  driveItem(vehicle) {
    let loading = this.loading.create({
      dismissOnPageChange: true,
      enableBackdropDismiss: false
    });
    loading.present();
    this.fire.setDispo(this.user, vehicle).then((key) => {
      this.geolocation.watchPosition().subscribe((position) => {
        this.profile.latitude = position.coords.latitude
        this.profile.longitude = position.coords.longitude
        this.fire.updateProfile(this.user, this.profile)
      })
      this.storage.set('VEHICLE_TYPE', vehicle.type).then(() => {
        loading.dismiss();
        this.navCtrl.push(WaitingListPage, { vehicle: vehicle, driver: this.user, user: this.user, profile: this.profile });
      })
    })
  }

  selectItem(vehicle) {
    this.action.create({
      title: vehicle.brand,
      buttons: [
        {
          text: 'Editer',
          handler: () => {
            this.navCtrl.push(AddVehiclePage, { user: this.user, vehicle: vehicle });
          }
        },
        {
          text: 'Supprimer',
          role: 'destructive',
          handler: () => {
            this.fire.removeVehicule(this.user, vehicle) ;
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    }).present();
  }
  
}

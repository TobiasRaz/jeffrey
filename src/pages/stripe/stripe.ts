import { Component, NgModule, ViewChild, ElementRef, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms' ;
import { NgxStripeModule, StripeService, Elements, Element as StripeElement, ElementOptions  } from 'ngx-stripe' ;

import { FormPage } from '../../classes/form-page';
import { User } from '../../models/user';
import { Race } from '../../models/race';
import { Stripe } from '../../models/stripe';
import { Storage } from '@ionic/storage';
import { StripeProvider } from '../../providers/stripe/stripe';

@IonicPage()
@Component({
  selector: 'page-stripe',
  templateUrl: 'stripe.html',
})
export class StripePage   implements OnInit {

  @ViewChild('card') cardRef:ElementRef;
  elements: Elements;
  card: StripeElement;
  elementsOptions:ElementOptions = {};
  stripeForm: FormGroup;
  messsage :any;
  user = {} as User;
  run = {} as Race;
  price: number;

  stripe_card:any;
  stripe_ref:any;
  cardSaved:any = {
    name: ''
  };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private stripeService: StripeService,
    private formBuilder:FormBuilder,
    private storage:Storage,
    private stripePrvd: StripeProvider,
    private toastCtrl: ToastController,
    private viewCtrl: ViewController
    ){
    this.user = this.navParams.get('user')
    this.price = this.navParams.get('price')
    this.run = this.navParams.get('run')
  }

  ionViewDidLoad() {
    try{
      this.storage.get(`STRIPE_CARD:${this.user.uid}`).then((cardSaved) => {
          if(cardSaved) {
            this.stripeForm.get('name').setValue(cardSaved.name)
          }
        })
    }catch(error){
      
    }
  }

  MountCard() {
    this.stripeService.elements({locale:'fr'}).subscribe((elements) => {
      this.elements = elements ;
      if(!this.card) {
        this.card = this.elements.create('card', {
          style: {
            base: {
              iconColor: '#666EE8',
                  color: '#FFF',
                  lineHeight: '40px',
                  fontWeight: 300,
                  fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                  fontSize: '18px',
                  '::placeholder': {
                    color: '#FFF',
                  }
            }
          },hidePostalCode: true,
          iconStyle: 'solid'
        })
      this.card.mount(this.cardRef.nativeElement);
      }
      // else
      //   console.log("ok") ;
    });
  }  

  ngOnInit() {
   this.storage
    this.stripeForm = this.formBuilder.group({
      name:['', [Validators.required]]
    });
    this.MountCard()
  }

  saveCard() {
    //console.log(this.card);
    if(this.stripeForm.valid) {
      try{
        return new Promise ((resolve) => {
          let card = {
            name: this.stripeForm.get('name').value
          }
          this.stripePrvd.generateToken(this.card, card.name)
          .then((token: any) => {
            if (token.error) {
              this.displayMessage(token.error.message)
            } else {
              //this.storage.set(`STRIPE_CARD:${this.user.uid}`, token)
              //.then(() => {
                this.stripeForm.get('name').setValue(card.name)
                if (!this.price)
                  this.displayMessage('Enregistré')
                else {
                  console.log(token)
                  this.pay(token)
                }
              //})
            }
          })
        })
      }catch(error) {
        this.displayMessage(error.message);
      }
    }else
      this.displayMessage('Nom sur la carte requis') ;
  }

  pay(token) {
      //this.storage.get(`STRIPE_CARD:${this.user.uid}`).then((token) => {
      this.stripePrvd.StripeCharge(token.token.id, this.price)
      .then((success)=> {
        this.displayMessage('Paiement effectué avec succès') ;
        this.viewCtrl.dismiss({ success: true, token: token.token.id, run: this.run })
      }
      //})
      )
      .catch(error => {
      this.displayMessage(error.message);
    })
  }

  displayMessage(messsage:any) {
      this.toastCtrl.create({
      duration: 3000,
      message: messsage,
      dismissOnPageChange: false
    }).present();
  }
}
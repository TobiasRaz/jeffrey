import { Component } from '@angular/core';
import { 
  IonicPage, 
  NavController, 
  NavParams, 
  MenuController, 
  Events, 
  LoadingController, 
  Loading,
  ModalController
  } from 'ionic-angular';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, MarkerIcon, MarkerOptions, Geocoder,
  Marker, LatLng, GeocoderRequest } from '@ionic-native/google-maps';
import { Keyboard } from '@ionic-native/keyboard'
import { Geolocation } from '@ionic-native/geolocation';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Profile } from '../../models/profile';
import { User } from '../../models/user';
import { Race } from '../../models/race';
import { Position } from '../../models/position';

import { YourRacePage} from '../../pages/your-race/your-race';
import { VotePage} from '../../pages/vote/vote';

import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

declare var google;
@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  map: GoogleMap;
  race: any;
  run: Race;
  vtypes: any[] =  [
    {
      'name': 'deux-roues',
      'icon': 'deux-roues',
      'title': 'DEUX-ROUES'
    },
    {
      'name': 'vtc',
      'icon': 'vtc',
      'title': 'VTC'
    },
    {
      'name': 'van',
      'icon': 'van',
      'title': 'VAN'
    },
    {
      'name': 'driver',
      'icon': 'driver',
      'title': 'Chauffeur'
    },
    {
      'name': 'concierge',
      'icon': 'concierge',
      'title': 'Concierge'
    },
    {
      'name': 'steeds',
      'icon': 'coursier',
      'title': 'Coursiers'
    },
  ];
  select_service: any = {};
  menu_open: boolean = true;
  vehicles = [];
  icon_size = []; 
  search: boolean = true; 
  driving: boolean = false; 
  driver: boolean = false;
  driver_marker: Marker;
  user = {} as User;
  profile = {} as Profile;
  isDriver: boolean = false;
  runing: boolean = false;
  vehicle: any;
  runDriver: any;
  loading: Loading;
  vehicle_markers: any = {};
  complete_input: string;
  start_marker: Marker;
  end_marker: Marker;
  suggest = {
    origin: {},
    destination: {}
  } as Race;
  polilynes = [];
  drivers = [];
  drivers_service = [];
  dispoAll$: Subscription; 
  dispoSelect$: Subscription; 
  destroyAll$: Subject<boolean> = new Subject<boolean>();
  destroySelect$: Subject<boolean> = new Subject<boolean>();
  displayDetail: boolean = true
  price: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private menuCtrl: MenuController, private googleMaps: GoogleMaps,
    private fire: FirebaseProvider,
    private events: Events, private geolocation: Geolocation,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private keyboard: Keyboard
    ) {
    this.menuCtrl.enable(true);
    this.race = this.navParams.get('race');
    this.run = this.navParams.get('run');
    this.user = this.navParams.get('user');
    this.profile = this.navParams.get('profile');
    this.vehicle = this.navParams.get('vehicle');
    this.isDriver = this.navParams.get('isDriver');
    this.runing = this.navParams.get('runing');
    this.complete_input = this.navParams.get('complete_input');
    this.price = this.navParams.get('price');

    if (this.price) {
      console.log(this.price)
      let voteModal = this.modalCtrl.create(VotePage, { user: this.user, price: this.price, run: this.run }, {
        enableBackdropDismiss:false
      })
      voteModal.present()
      delete this.run;
    }

    this.events.subscribe('close:loading', () => {
      this.swhitchToFistComponent();
    })
 
    this.icon_size['bike'] = {'width':30,'height':20,'x':15,'y':20};
    this.icon_size['deux-roues'] = {'width':41,'height':19,'x':20,'y':19};
    this.icon_size['van'] = {'width':69,'height':25,'x':34,'y':25};
    this.icon_size['vtc'] = {'width':41,'height':22,'x':20,'y':11};
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      enableBackdropDismiss: false,
      dismissOnPageChange: true
    })
    this.loading.present();
  }

  private swhitchToFistComponent() {
    this.select_service = false;
    this.menu_open = true ; 
  }

  private geocodeRun(position) {
    Geocoder.geocode({ position: position }).then((name) => {
      alert(JSON.stringify(name));
    })
  }

  ionViewDidEnter() {
    this.keyboard.close();
  }

  ionViewDidLoad() {
    let target = [];
    if (this.run) {
      let start = {
        lat: this.run.origin.latitude,
        lng: this.run.origin.longitude
      }
      target.push(start)
      this.loadMap(target, 18, 'map_canvas_short');
      /**this.traceRace()
      if (this.runing) {
        this.select_service.title = this.run.type;
        this.select_service.name = this.run.type.toLowerCase();
        this.select_service.icon = this.run.type.toLowerCase();
        this.showDriver(this.run.driver, this.select_service.type)
      }/**/
      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        if (this.isDriver) {
          this.destroySelect$.next(true)
          this.destroyAll$.next(true)
          this.destroySelect$.unsubscribe()
          this.destroyAll$.unsubscribe()
        }

        if (this.run.origin.id && this.run.destination.id) {
          this.traceRace()
          if (this.runing) {
            this.select_service.name = this.run.type.toLowerCase();
            this.select_service.icon = this.run.type.toLowerCase();
            this.select_service.title = this.run.type;
            this.showDriver(this.run.driver, this.select_service.type)
          }
        }
        else
          this.mark_starting(start)

        if (!this.runing) {
          this.removeDriverMark();
          this.showDrivers();
        }
      })
    } else {
      this.loadMap(new LatLng(46.227638,2.213749000000007), 5);

      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        this.removeDriverMark();
        this.showDrivers();
      })
    }
  }

  loadMap(target?: any, zoom: number = 18, canvas: string = 'map_canvas') {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: target,
        zoom: zoom,
        tilt: 30
      },
      controls: {
        compass: false,
        mapToolbar: false
      }
    };
    this.map = this.googleMaps.create(canvas, mapOptions);
  }

  showDrivers(category?: string) {
    this.removeDriverMark();
    console.log('show');
    let _vehicles = [];
    let _v: any;
    if (!category) {
      if (this.dispoSelect$) {
        this.destroySelect$.next(true)
      }
      this.dispoAll$ = this.fire.getAllDispo()
      .takeUntil(this.destroyAll$)
      .subscribe((vehicles) => {
        this.removeDriverMark();
        if (!vehicles) {
          this.search = false
          this.destroyAll$.next(true)
        }
        for (var categorie in vehicles) {
          for (var driver in vehicles[categorie]) {
            for (var key in vehicles[categorie][driver]) {
              this.showDriver(driver, categorie);
            }
          }
        }
      })
    }
    else {
      if (this.dispoAll$) {
        this.destroyAll$.next(true)
      }
      this.dispoSelect$ = this.fire.getDispoChange(category)
      .takeUntil(this.destroySelect$)
      .subscribe((vehicles) => {
        this.removeDriverMark()
        if (!vehicles) {
          this.search = false
          this.destroySelect$.next(true)
        }
        for (var driver in vehicles) {
          for (var key in vehicles[driver]) {
            if (this.select_service && category && category == this.select_service.title)
              this.showDriver(driver, category, vehicles[driver][key].vehicle);
          }
        }
      })
    }


  }

  removeDriverMark() {
    console.log('remove');
    for (var key in this.vehicle_markers) {
      this.vehicle_markers[key].remove();
      delete this.vehicle_markers[key]
    }
    this.drivers = [];
    this.drivers_service = null
  }

  showDriver(driver_uid: string, categorie: string, vehicle_uid?: string) {
    let ref$ = this.fire.getProfileChange(driver_uid)
    .takeUntil(this.destroyAll$)
    .takeUntil(this.destroySelect$)
    ref$.subscribe((_driver:any) => {
      let key = Object.keys(_driver)[0]
      let driver = _driver[key];
      driver.uid = driver_uid
      if (vehicle_uid) {
        driver.vehicle = {}
        driver.vehicle.id = vehicle_uid
      }
      if (this.runing) {
        this.runDriver = _driver
        this.traceRaceDriver(driver)
      } else {
        this.markDriver(driver, categorie);
      }
    })
  }

  markDriver(driver, categorie: string) {
    console.log('mark');
    categorie = categorie.toLowerCase();

    if (!this.drivers[categorie])
      this.drivers[categorie] = []

    this.drivers[categorie].push(driver);

    if (this.select_service && this.drivers[this.select_service.name])
      this.drivers_service = this.drivers[this.select_service.name]

    let position = new LatLng(driver.latitude, driver.longitude);

    if (!this.vehicle_markers[driver.uid]) {
      let icon_vehicle: any = this.icon_size[categorie];
      let icon : MarkerIcon = {
        url: 'assets/icon/' + categorie + '_icon.png',
        size: { width: icon_vehicle.width, height: icon_vehicle.height }
      }
      let _marker: MarkerOptions = {
        position: position,
        icon: icon
      }
      this.map.addMarker(_marker)
      .then((marker) => {
        this.vehicle_markers[driver.uid] = marker
      })
      .catch((e) => alert(e))
    } else {
      this.vehicle_markers[driver.uid].setPosition(position)
    }
  }

  private traceRaceDriver(driver) {
    this.traceRace(driver, this.run.origin)
  }

  private traceRace(driver?:any, destination?:Position, color:string = '#00ffd8') {
    if (this.polilynes['race'] && !driver)
      this.polilynes['race'].remove()

    if (this.polilynes['driver'] && driver)
      this.polilynes['driver'].remove()

    let directionsService = new google.maps.DirectionsService();

    let origin: Position;

    if (!driver) {
      origin = this.run.origin
      destination = this.run.destination
    } else 
      origin = driver

    let request = {
      origin: new google.maps.LatLng(origin.latitude, origin.longitude),
      destination: new google.maps.LatLng(destination.latitude, destination.longitude),
      unitSystem: google.maps.UnitSystem.METRIC,
      travelMode: google.maps.TravelMode.DRIVING,
    };

    let direction = new Promise((resolve, reject) => {

      directionsService.route(request, function(result, status) {
        if (status == "OK") {
         resolve(result);
        }
      });
    });

    direction.then((response: any) => {
      let polilynes = [];
      response.routes[0].overview_path.forEach((position) => {
        let polilyne = { lat: position.lat(), lng: position.lng() }
        polilynes.push(polilyne);
      });
      let leg = response.routes[0].legs[0];
      let start = leg.start_location;
      let end = leg.end_location;
      let target = [{
          lat: start.lat(),
          lng: start.lng()
        }, {
          lat: end.lat(),
          lng: end.lng()
        }]

      let mark$: any = {};

      if (!driver) {
        mark$.start = this.mark_starting(start)
        mark$.end = this.mark_ending(end)
      } else {
        driver.latitude = start.lat()
        driver.longitude = start.lng()
        this.markDriver(driver, this.select_service.title)
        color = '#051a27'
        this.run.duration = Math.round(leg.duration.value/60);
      }

      this.map.addPolyline({
        points: polilynes,
        'color' : color,
        'width': 10,
        'geodesic': true,
      }).then((pol) => {
        if (driver)
          this.polilynes['driver'] = pol
        else
          this.polilynes['race'] = pol
      });

      if (mark$.start && mark$.end) {
        mark$.start.then(() => {
          mark$.end.then(() => {
            this.map.moveCamera({
              target: target,
              zoom: 18
            })
          })
        })
      } else {
        this.map.moveCamera({
          target: target,
          zoom: 18
        })
      }

      
    });
  }

  markResult(position) {
    this.keyboard.close();
    let location = position.geometry.location
    this.suggest.origin = {
      id: position.id,
      latitude: location.lat(),
      longitude: location.lng(),
      placeName: position.name
    } 
    this.mark_starting(location)
  }

  validateAddress() {
    let dom = document.getElementById('map_canvas_short')
    this.map.setDiv(dom)
    this.run = this.suggest;
  }

  public mark_starting(start) {
    return new Promise((resolve) => {
      if (this.start_marker)
        this.start_marker.remove();
      let icon_start : MarkerIcon = {
        url: './assets/icon/start.png',
        size: { width: 32, height:47 }
      }
      let marker_start: MarkerOptions = {
        position: start,
        icon: icon_start
      }
      this.map.addMarker(marker_start)
      .then((marker: Marker) => {
        resolve(true)
        this.start_marker = marker;
        this.map.moveCamera({
          target: start,
          zoom: 18
        })
      })
      .catch((e) => alert(e))
    })
  }

  public mark_ending(end) {
    return new Promise((resolve) => {
      if (this.end_marker)
        this.end_marker.remove();
      let icon_end : MarkerIcon = {
        url: './assets/icon/end.png',
        size: { width: 32, height:47 }
      }
      let marker_end: MarkerOptions = {
        position: end,
        icon: icon_end
      }
      this.map.addMarker(marker_end)
      .then((marker: Marker) => {
        resolve(true)
        this.end_marker = marker;
        this.map.moveCamera({
          target: end,
          zoom: 18
        })
      })
      .catch((e) => alert(e))
    })
  }

  toggle_service_menu() {
    this.menu_open = !this.menu_open;
  }

  confirmOrder(yes: boolean) {
    if (yes)
      this.showDrivers(this.select_service.title)
    else 
      this.swhitchToFistComponent()
    this.displayDetail = false

  }

  changeCategory(service: any) {
    this.search = true
    this.displayDetail = true
    this.run.type = service.title
    this.removeDriverMark()
    this.toggle_service_menu()
    this.select_service = service
  }

  moovCamera(driver) {
    let position = new LatLng(driver.latitude, driver.longitude)
    let origin = new LatLng(this.run.origin.latitude, this.run.origin.longitude)
    this.map.moveCamera({
      target: [ position, origin ],
      zoom: 18
    })
  }
  
  driverDispo(dispo: boolean) {
    this.run.driver = this.user.uid
    if (!dispo) {
      this.fire.removeCallList(this.run).then(() => {
        this.navCtrl.pop();
      })
    } else {
      this.fire.createRuning(this.run, this.vehicle)
      this.driving = true;
    }
  }

  yourRace() {
    let yourRaceModal = this.modalCtrl.create(YourRacePage, { run : this.run });
    yourRaceModal.onDidDismiss((run: Race) => {
      if (run) {
        if (run.origin.id)
          this.run.origin = run.origin

        if (run.destination.id)
          this.run.destination = run.destination

        if (run.origin.id && run.destination.id) {
          this.traceRace()
        }
      }
    })
    yourRaceModal.present();
  }

}

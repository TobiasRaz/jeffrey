import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController,ToastController,
  MenuController, Loading } from 'ionic-angular';
import { MapPage } from '../map/map';
import { User } from '../../models/user';
import { Storage } from '@ionic/storage';

declare var google;
@IonicPage()
@Component({
  selector: 'page-home-state',
  templateUrl: 'home-state.html',
})
export class HomeStatePage {

  domicile: any = {};
  user = {} as User;
  loading: Loading;

  constructor(private menuCtrl: MenuController,
    private alertCtrl: AlertController, private platform : Platform, 
    public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private storage: Storage,private toastCtrl: ToastController
    ) {
    this.user = this.navParams.get('user');
    this.menuCtrl.enable(true);
  }

  ionViewDidEnter() {
    this.prepareAutocomplete();
    this.storage.get('HOME:state').then((domiciles) => {
      if (domiciles) {
        this.domicile = JSON.parse(domiciles);
      }
    })
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      enableBackdropDismiss: false,
      dismissOnPageChange: true
    })
    this.loading.present()
  }
  save(adresse) {
    this.storage.set('HOME:state', JSON.stringify(adresse)).then(() => {
      this.presentError('Enregistré');
       this.navCtrl.pop();
    })
  }
  public presentError(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
    });

    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }

  prepareAutocomplete() {
    this.verifyGoogle().then(() => {
      let orgin_input = <HTMLInputElement>document.querySelector('#domicile input');
      let autocomplete: any = {};
      let options = { componentRestrictions: { country: "fr" }};
      autocomplete.domicile = new google.maps.places.Autocomplete(orgin_input, options);
      google.maps.event.addListener(autocomplete.domicile, 'place_changed', () => {
        this.domicile = autocomplete.domicile.getPlace();
      })
    });
  }

  public viewMap() {
    this.presentLoading()
    if (this.domicile.id) {
      let params = {
        domicile_name: this.domicile.name,
        domicile_lat:  this.domicile.geometry.location.lat(),
        domicile_lng:  this.domicile.geometry.location.lng(),
      };
      let directionsService = new google.maps.DirectionsService();

      let request = {
        domicile: new google.maps.LatLng(params.domicile_lat, params.domicile_lng),
        unitSystem: google.maps.UnitSystem.METRIC,
        travelMode: google.maps.TravelMode.DRIVING,
      };

      let direction = new Promise((resolve, reject) => {

        directionsService.route(request, function(result, status) {
          if (status == "OK") {
           resolve(result);
          } else {
            reject(result);
          }
        });
      });

      direction.then((result) => {
        this.loading.dismiss()
        this.navCtrl.push(MapPage, { race : params, user: this.user });
      }).catch((e) => {
        let alert = {
          title : 'Erreur',
          subTitle: 'Ce trajet est impossible',
          buttons: [
            {
              text: 'Changer'
            }
          ]
        }
        this.showAlert(alert);
      })
    } else {
      this.loading.dismiss();
    }
  }

  protected verifyGoogle() {
    return new Promise((resolve) => {
      if (typeof(google) == 'undefined') {
        let alert: {} = {
          title : 'Oh!',
          subTitle: 'L\'API de Google Map n\'est pas fonctionnel',
          message: 'Veuillez vérifier votre connexion avant de réessayer',
          buttons: [
            {
              text: 'Réessayer',
              handler: () => {
                this.verifyGoogle();
              }
            },
            {
              text: 'Quitter',
              handler: () => {
                this.platform.exitApp();
              }
            },
          ],
          enableBackdropDismiss: false,
        }

       this.showAlert(alert);
       resolve(false);
      } else {
        resolve(true);
      }
    });
  }

  protected showAlert(alert) {
    this.alertCtrl.create(alert).present();
  }

}

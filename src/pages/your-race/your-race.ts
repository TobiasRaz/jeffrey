import { Component } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController,
  MenuController, Loading, ViewController } from 'ionic-angular';
import { MapPage } from '../map/map';
import { Race } from '../../models/race';

declare var google;
@IonicPage()
@Component({
  selector: 'page-your-race',
  templateUrl: 'your-race.html',
})
export class YourRacePage {

  run = {
    origin: {}, 
    destination: {}
  } as Race;
  loading: Loading;

  constructor(private menuCtrl: MenuController,
    private alertCtrl: AlertController, private platform : Platform, 
    public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    private keyboard: Keyboard,
    ) {
    this.run = this.navParams.get('run');
    this.menuCtrl.enable(true);
  }

  ionViewDidEnter() {
    this.prepareAutocomplete();
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      enableBackdropDismiss: false,
      dismissOnPageChange: true
    })
    this.loading.present()
  }

  prepareAutocomplete() {
  	this.verifyGoogle().then(() => {
  	  let orgin_input = <HTMLInputElement>document.querySelector('#origin input');
  	  let autocomplete: any = {};
      let options = { componentRestrictions: { country: "fr" }};
      //delete options.componentRestrictions;
      autocomplete.origin = new google.maps.places.Autocomplete(orgin_input, options);
      google.maps.event.addListener(autocomplete.origin, 'place_changed', () => {
        let origin = autocomplete.origin.getPlace();
        this.run.origin = {
          id: origin.id,
          placeName: origin.name,
          latitude: origin.geometry.location.lat(),
          longitude: origin.geometry.location.lng()
        };
        this.verifyRace();
      })
      let destination_input = <HTMLInputElement>document.querySelector('#destination input');
  	  autocomplete.destination = new google.maps.places.Autocomplete(destination_input, options);
  	  google.maps.event.addListener(autocomplete.destination, 'place_changed', () => {
        let destination = autocomplete.destination.getPlace();
        this.run.destination = {
          id: destination.id,
          placeName: destination.name,
          latitude: destination.geometry.location.lat(),
          longitude: destination.geometry.location.lng()
        };
        this.verifyRace();
    	})
	  });
  }

  public verifyRace() {
    this.presentLoading()
    if (this.run.origin && this.run.destination && this.run.origin.id != this.run.destination.id) {
      let directionsService = new google.maps.DirectionsService();

      let request = {
        origin: new google.maps.LatLng(this.run.origin.latitude, this.run.origin.longitude),
        destination: new google.maps.LatLng(this.run.destination.latitude, this.run.destination.longitude),
        unitSystem: google.maps.UnitSystem.METRIC,
        travelMode: google.maps.TravelMode.DRIVING,
      };

      let direction = new Promise((resolve, reject) => {

        directionsService.route(request, function(result, status) {
          if (status == "OK") {
           resolve(result);
          } else {
            reject(result);
          }
        });
      });

      direction.then((result) => {
        if (!this.run.origin.id)
          this.run.origin.id = "origin_ok"
        if (!this.run.destination.id)
          this.run.destination.id = "destination_ok"
        this.loading.dismiss()
        let run = {
          origin: this.run.origin,
          destination: this.run.destination
        } as Race;
        this.keyboard.close();
        this.viewCtrl.dismiss(run)
      }).catch((e) => {
        if (this.run.origin.id && this.run.destination.id)
          this.alertError()
        this.loading.dismiss();
      })
    } else {
      if (this.run.origin.id == this.run.destination.id)
        this.alertError()

      this.loading.dismiss();
    }
  }

  private alertError() {
    let alert = {
      title : 'Erreur',
      subTitle: 'Ce trajet est impossible',
      buttons: [
        {
          text: 'Changer',
          handler: () => {
            this.loading.dismiss()
          }
        }
      ]
    }
    this.showAlert(alert);
  }

  protected verifyGoogle() {
    return new Promise((resolve) => {
      if (typeof(google) == 'undefined') {
        let alert: {} = {
          title : 'Oh!',
          subTitle: 'L\'API de Google Map n\'est pas fonctionnel',
          message: 'Veuillez vérifier votre connexion avant de réessayer',
          buttons: [
            {
              text: 'Réessayer',
              handler: () => {
                this.verifyGoogle();
              }
            },
            {
              text: 'Quitter',
              handler: () => {
                this.platform.exitApp();
              }
            },
          ],
          enableBackdropDismiss: false,
        }

       this.showAlert(alert);
       resolve(false);
      } else {
        resolve(true);
      }
    });
  }

  protected showAlert(alert) {
    this.alertCtrl.create(alert).present();
  }

}

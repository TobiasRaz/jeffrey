import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Profile } from '../../models/profile';
import { User } from '../../models/user';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Camera, CameraOptions } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  
  profile = {} as Profile;
  user = {} as User;
  item: string;
  uid: string;
  myPhotoURL:string = "assets/images/client.png" ;
  edit = {
    firstName: true,
    name: true,
    phoneNumber: true 
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, 
    private fire: FirebaseProvider, private camera: Camera) {
  	this.profile = this.navParams.get('profile')
  	this.user = this.navParams.get('user')
  }

  updateProfile(profile: Profile) {
  	this.fire.updateProfile(this.user, profile).then(() => {
      this.edit.firstName = true;
      this.edit.name = true;
      this.edit.phoneNumber = true;
      this.toastCtrl.create({
        duration: 3000,
        message: 'Enregistré',
        dismissOnPageChange: false
      }).present();
  	})
  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 50,
      targetHeight: 600,
      targetWidth: 600,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    
    this.camera.getPicture(options).then((imageData) => {
      let image = `data:image/jpeg;base64,${imageData}`;
      this.fire.storeImageProfile(image, this.user).then((url: string) => {
        this.profile.photo_url = url;
        this.updateProfile(this.profile);
      }).catch((e) => {
        alert(JSON.stringify(e))
      })
    })
  }

  isReadOnly(item: string) {
    return this.edit[item];
  }

  toogleInput(item: string) {
    this.edit[item] = !this.edit[item]
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController,ToastController,
  MenuController, Loading } from 'ionic-angular';
import { MapPage } from '../map/map';
import { User } from '../../models/user';
import { Storage } from '@ionic/storage';

declare var google;
@IonicPage()
@Component({
  selector: 'page-other-place',
  templateUrl: 'other-place.html',
})
export class OtherPlacePage {

  other: any = {};
  user = {} as User;
  loading: Loading;

  constructor(private menuCtrl: MenuController,
    private alertCtrl: AlertController, private platform : Platform, 
    public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private storage: Storage,private toastCtrl: ToastController
    ) {
    this.user = this.navParams.get('user');
    this.menuCtrl.enable(true);
  }

  ionViewDidEnter() {
    this.prepareAutocomplete();
     this.storage.get('OTHER:place').then((others) => {
      if (others) {
        this.other = JSON.parse(others);
      }
    })
  }

   save(other) {
    this.storage.set('OTHER:place', JSON.stringify(other)).then(() => {
      this.presentError('Enregistré');
       this.navCtrl.pop();
    })
  }
  public presentError(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
    });

    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }
  presentLoading() {
    this.loading = this.loadingCtrl.create({
      enableBackdropDismiss: false,
      dismissOnPageChange: true
    })
    this.loading.present()
  }

  prepareAutocomplete() {
    this.verifyGoogle().then(() => {
      let orgin_input = <HTMLInputElement>document.querySelector('#other input');
      let autocomplete: any = {};
      let options = { componentRestrictions: { country: "fr" }};
      autocomplete.other = new google.maps.places.Autocomplete(orgin_input, options);
      google.maps.event.addListener(autocomplete.other, 'place_changed', () => {
        this.other = autocomplete.other.getPlace();
      })
    });
  }

  public viewMap() {
    this.presentLoading()
    if (this.other.id) {
      let params = {
        other_name: this.other.name,
        other_lat:  this.other.geometry.location.lat(),
        other_lng:  this.other.geometry.location.lng(),
      };
      let directionsService = new google.maps.DirectionsService();

      let request = {
        other: new google.maps.LatLng(params.other_lat, params.other_lng),
        unitSystem: google.maps.UnitSystem.METRIC,
        travelMode: google.maps.TravelMode.DRIVING,
      };

      let direction = new Promise((resolve, reject) => {

        directionsService.route(request, function(result, status) {
          if (status == "OK") {
           resolve(result);
          } else {
            reject(result);
          }
        });
      });

      direction.then((result) => {
        this.loading.dismiss()
        this.navCtrl.push(MapPage, { race : params, user: this.user });
      }).catch((e) => {
        let alert = {
          title : 'Erreur',
          subTitle: 'Ce trajet est impossible',
          buttons: [
            {
              text: 'Changer'
            }
          ]
        }
        this.showAlert(alert);
      })
    } else {
      this.loading.dismiss();
    }
  }

  protected verifyGoogle() {
    return new Promise((resolve) => {
      if (typeof(google) == 'undefined') {
        let alert: {} = {
          title : 'Oh!',
          subTitle: 'L\'API de Google Map n\'est pas fonctionnel',
          message: 'Veuillez vérifier votre connexion avant de réessayer',
          buttons: [
            {
              text: 'Réessayer',
              handler: () => {
                this.verifyGoogle();
              }
            },
            {
              text: 'Quitter',
              handler: () => {
                this.platform.exitApp();
              }
            },
          ],
          enableBackdropDismiss: false,
        }

       this.showAlert(alert);
       resolve(false);
      } else {
        resolve(true);
      }
    });
  }

  protected showAlert(alert) {
    this.alertCtrl.create(alert).present();
  }

}

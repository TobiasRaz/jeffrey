import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
 
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FormPage } from '../../classes/form-page';
import { Profile } from '../../models/profile';
import { User } from '../../models/user';
import { FirebaseProvider } from '../../providers/firebase/firebase';
 
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage extends FormPage {
  protected form: FormGroup;
  protected selectOptions : any[];
  protected selectDefault :any ;
  profile = {} as Profile;
  user = {} as User;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, public toastCtrl: ToastController, private formBuilder: FormBuilder,
    private fire: FirebaseProvider,
    ) {
    super(toastCtrl);
    this.form = this.formBuilder.group({
      name: ['',Validators.required],
      first_name: ['',Validators.required],
      email: ['',Validators.compose([Validators.pattern(/^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/),Validators.required])],
      phone: ['',Validators.compose([Validators.pattern(/^[\d]+$/), Validators.required])],
      password: ['',Validators.compose([Validators.required, Validators.minLength(6)])],
      repassword: ['',Validators.required]
    }, {validator : this.matchingPasswords('password', 'repassword')});
  }
 
  protected matchingPasswords(passwordKey: string, confirmPasswordKey: string)
  {
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
 
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }
  public register() {
    if (this.form.valid) {
      let value = this.form.value;
      this.profile = {
        firstName: value.first_name,
        name: value.name,
        phoneNumber: value.phone
      }
      this.user = {
        email: value.email,
        password: value.password
      }
      this.fire.register(this.user, this.profile)
      .then(() => {
        this.navCtrl.pop();
        this.presentError('Votre compte a été créer avec succès')})
      .catch((e) => {
        switch (e.code) {
          case "auth/email-already-in-use":
            this.presentError('L\'adresse e-mail est déja utilisée par un autre compte');
            break;
        }
      });
    } else {
      if (this.form.value.password != this.form.value.repassword)
        this.presentError('Les mots de passe ne sont pas identiques');
      this.reportError();
    }
  }
 
  public getMessageError(key: string, validator: string) {
    let message_error: any = {};
    message_error.required = {};
    message_error.pattern = {};
    message_error.minlength = {};
    message_error.required.name = "Le nom est requis!";
    message_error.required.first_name = "Le prénom est requis!";
    message_error.required.email = "L'address e-mail est requis!";
    message_error.required.phone = "Le numéro téléphone est requis!";
    message_error.required.password = "Le mots de passe est requis!";
    message_error.minlength.password = "Le mots de passe est trop court!";
    message_error.required.repassword = "Confimer votre mots de passe!";
    message_error.pattern.email = "Address e-mail invalid!";
    message_error.pattern.phone = "Numéro téléphone incorrecte!";
    return message_error[validator][key];
  }
 
}
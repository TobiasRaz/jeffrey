import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Race } from '../../models/race';



@IonicPage()
@Component({
  selector: 'page-racing-detail',
  templateUrl: 'racing-detail.html',
})
export class RacingDetailPage {
  course = {} as Race;
  driver = {};

  constructor(public navCtrl: NavController,
  	public navParams: NavParams,
 	public FirProvider: FirebaseProvider
  	) {
  	this.course = this.navParams.get("course") ;
	  this.FirProvider.getProfile(this.course.driver).then((driver) => {
  	  let driver_uid :string = Object.keys(driver)[0];
  	  let driver_: {} = driver[driver_uid];
  	  this.driver = {
  	  	name: driver_['name'],
  	  	firstName: driver_['firstName']
  	  }
    })
  }

  ionViewDidLoad() {
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StripePage } from '../stripe/stripe' ;
import { User } from '../../models/user';

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  user = {} as User;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.user = this.navParams.get('user');
  }

  stripe() {
  	this.navCtrl.push(StripePage, { user: this.user }) ;
  }
}

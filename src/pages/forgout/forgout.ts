import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormPage } from '../../classes/form-page';
import { User } from '../../models/user';
import { FirebaseProvider } from '../../providers/firebase/firebase';

@IonicPage()
@Component({
  selector: 'page-forgout',
  templateUrl: 'forgout.html',
})
export class ForgoutPage extends FormPage {

  user = {} as User;
  protected form : FormGroup;
  constructor(public navCtrl: NavController, private firePrvd: FirebaseProvider,
              public navParams: NavParams,
              public formBuilder : FormBuilder, private menuCtrl: MenuController,
              public toastCtrl: ToastController) {
  	 	super(toastCtrl);
	    this.menuCtrl.enable(false);
	    this.form = formBuilder.group({
	        login: ['', Validators.compose([Validators.pattern(/^([\w\-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})$/), Validators.required])]
	    });
  }

  sendMail(email){
    if (this.form.valid) {
      this.firePrvd.sendResetPass(this.form.value.login)
      .then(() => this.presentError('Une e-mail de validation vous été envoyer merci'))
      .catch((e) => {
        switch (e.code) {
          case "auth/user-not-found":
            this.presentError('Il n\'y a pas d\'enregistrement utilisateur correspondant à cet identifiant. L\'utilisateur a peut-être été supprimé');
            break;
          
          case "auth/internal-error":
              try {
                let error = JSON.parse(e.message).error.message
                if (error == "RESET_PASSWORD_EXCEED_LIMIT")
                  this.presentError('Vous avez dépassé la limite de demande de réinitialisation de mots de passe');
              } catch(jsE) {

              }
            break;
        }
      });
    } else {
      this.reportError();
    }
  }

   public getMessageError(key: string, validator: string) {
    let message_errors = [];
    message_errors['login'] = [];
    message_errors['login']['required'] = 'Adresse e-mail requis!';
    message_errors['login']['pattern'] = 'Adresse e-mail invalid';
    return message_errors[key][validator];
  }

}

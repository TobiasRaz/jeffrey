import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController,ToastController,
  MenuController, Loading } from 'ionic-angular';
import { MapPage } from '../map/map';
import { User } from '../../models/user';
import { Storage } from '@ionic/storage';

declare var google;
@IonicPage()
@Component({
  selector: 'page-office-param',
  templateUrl: 'office-param.html',
})
export class OfficeParamPage {

  office: any = {};
  user = {} as User;
  loading: Loading;

  constructor(private menuCtrl: MenuController,
    private alertCtrl: AlertController, private platform : Platform, 
    public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private storage: Storage,private toastCtrl: ToastController
    ) {
    this.user = this.navParams.get('user');
    this.menuCtrl.enable(true);
  }

  ionViewDidEnter() {
    this.prepareAutocomplete();
    this.storage.get('OFFICE:param').then((offices) => {
      if (offices) {
        this.office = JSON.parse(offices);
      }
    })
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      enableBackdropDismiss: false,
      dismissOnPageChange: true
    })
    this.loading.present()
  }

   save(office) {
    this.storage.set('OFFICE:param', JSON.stringify(office)).then(() => {
      this.presentError('Enregistré');
       this.navCtrl.pop();
    })
  }
  public presentError(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
    });

    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }

  prepareAutocomplete() {
    this.verifyGoogle().then(() => {
      let orgin_input = <HTMLInputElement>document.querySelector('#office input');
      let autocomplete: any = {};
      autocomplete.office = new google.maps.places.Autocomplete(orgin_input);
      google.maps.event.addListener(autocomplete.office, 'place_changed', () => {
        this.office = autocomplete.office.getPlace();
      })
    });
  }

  public viewMap() {
    this.presentLoading()
    if (this.office.id) {
      let params = {
        office_name: this.office.name,
        office_lat:  this.office.geometry.location.lat(),
        office_lng:  this.office.geometry.location.lng(),
      };
      let directionsService = new google.maps.DirectionsService();

      let request = {
        office: new google.maps.LatLng(params.office_lat, params.office_lng),
        unitSystem: google.maps.UnitSystem.METRIC,
        travelMode: google.maps.TravelMode.DRIVING,
      };

      let direction = new Promise((resolve, reject) => {

        directionsService.route(request, function(result, status) {
          if (status == "OK") {
           resolve(result);
          } else {
            reject(result);
          }
        });
      });

      direction.then((result) => {
        this.loading.dismiss()
        this.navCtrl.push(MapPage, { race : params, user: this.user });
      }).catch((e) => {
        let alert = {
          title : 'Erreur',
          subTitle: 'Ce trajet est impossible',
          buttons: [
            {
              text: 'Changer'
            }
          ]
        }
        this.showAlert(alert);
      })
    } else {
      this.loading.dismiss();
    }
  }

  protected verifyGoogle() {
    return new Promise((resolve) => {
      if (typeof(google) == 'undefined') {
        let alert: {} = {
          title : 'Oh!',
          subTitle: 'L\'API de Google Map n\'est pas fonctionnel',
          message: 'Veuillez vérifier votre connexion avant de réessayer',
          buttons: [
            {
              text: 'Réessayer',
              handler: () => {
                this.verifyGoogle();
              }
            },
            {
              text: 'Quitter',
              handler: () => {
                this.platform.exitApp();
              }
            },
          ],
          enableBackdropDismiss: false,
        }

       this.showAlert(alert);
       resolve(false);
      } else {
        resolve(true);
      }
    });
  }

  protected showAlert(alert) {
    this.alertCtrl.create(alert).present();
  }

}

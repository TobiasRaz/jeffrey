import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { User } from '../../models/user';
import { Race } from '../../models/race';
import { Profile } from '../../models/profile';
import { MapPage } from '../map/map';

import { FirebaseProvider } from '../../providers/firebase/firebase';
import { AngularFireDatabase } from 'angularfire2/database';

@IonicPage()
@Component({
  selector: 'page-waiting-list',
  templateUrl: 'waiting-list.html',
})
export class WaitingListPage {

  vehicle: any;
  driver = {} as User;
  user = {} as User;
  profile = {} as Profile;
  clients = [];

  constructor(
  	public navCtrl: NavController, public navParams: NavParams, private loading: LoadingController,
    private storage: Storage, private fire: FirebaseProvider, private database: AngularFireDatabase
    ) {
    this.vehicle = this.navParams.get('vehicle');
    this.driver = this.navParams.get('driver');
    this.user = this.navParams.get('user');
  	this.profile = this.navParams.get('profile');
  }

  /*ionViewCanEnter() {
    this.clients = [];
  }*/

  ionViewDidLoad() {
    this.database.object(`call-list/${this.user.uid}`).valueChanges().subscribe((clients) => {
      this.clients = [];
      for (var key in clients) {
        let client = clients[key];
        client.key = key;
        this.database.list(`profile/${client.client}`).valueChanges().subscribe((profile: any) => {
          let race: Race = {
            origin: {
              latitude: client.origin_lat,
              longitude: client.origin_lng,
              placeName: client.origin_name,
              id: "origin_ok"
            },
            destination: {
              latitude: client.destination_lat,
              longitude: client.destination_lng,
              placeName: client.destination_name,
              id: "destination_ok"
            },
            key: client.key,
            client: client.client
          } as Race
          profile[0].race = race;
          this.clients.push(profile[0]);
          ;
        })
      }
    });
  }

  pop() {
    let loading = this.loading.create({
      enableBackdropDismiss: false
    })
    loading.present();
    this.storage.remove('VEHICLE_TYPE').then(() => {
      loading.dismiss()
      this.fire.removeDispo(this.driver.uid, this.vehicle).then(() => {
        this.navCtrl.pop();
      })
    })

  }

  check(client) {
    this.navCtrl.push(MapPage, { run: client.race, isDriver: true, user: this.user, vehicle: this.vehicle, profile: this.profile });
  }

}

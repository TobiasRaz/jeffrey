import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Profile } from '../../models/profile';
import { User } from '../../models/user';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { LoginPage } from '../login/login';
import { EventPage } from '../event/event';
import { ProfilBusinessPage } from '../profil-business/profil-business';
import { AdressProPage } from '../adress-pro/adress-pro';
import { HomeStatePage } from '../home-state/home-state';
import { OfficeParamPage } from '../office-param/office-param';
import { OtherPlacePage } from '../other-place/other-place';
import { ConfParamPage } from '../conf-param/conf-param';
import { ProfilePage } from '../profile/profile';

@IonicPage()
@Component({
  selector: 'page-parametres',
  templateUrl: 'parametres.html',
})
export class ParametresPage {
  
  profile = {} as Profile;
  user = {} as User;
  item: string;
  uid: string;
  myPhotoURL:string = "assets/images/client.png" ;
  profile_p: any = ProfilePage;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private fire: FirebaseProvider) {
  	this.profile = this.navParams.get('profile')
  	this.user = this.navParams.get('user')
  }

  adressPro(){
    this.navCtrl.push(AdressProPage);
  }

  homeState(){
    this.navCtrl.push(HomeStatePage);
  }

  officeParam(){
    this.navCtrl.push(OfficeParamPage);
  }

  otherPlace(){
    this.navCtrl.push(OtherPlacePage);
  }

  calandarParam(){
    this.navCtrl.push(EventPage);
  }

  profilBussines(){
    this.navCtrl.push(ProfilBusinessPage);
  }

  confParam(){
    this.navCtrl.push(ConfParamPage);
  }
  
  logout(){
    this.fire.logout().then(() => {
      this.navCtrl.setRoot(LoginPage);
    })
  }
  
  openProfile(){
		this.navCtrl.push(ProfilePage, { profile: this.profile, user: this.user });
  }

}

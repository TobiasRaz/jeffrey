export interface Profile {
	photo_url?: string,
	item?: string,
	firstName: string,
	name: string,
	phoneNumber: string,
	latitude?: number,
	longitude?: number
}
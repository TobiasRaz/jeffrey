export interface Vehicle {
  brand: string;
  placeNb: number;
  type: string;
  driver: string;
  key?: string;
  picture_url?: string;
}
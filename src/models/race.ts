import { Position } from './position';
export interface Race { 
  key?: string,
  origin: Position,
  destination: Position,
  client?: string,
  driver?: string,
  type?: string,
  date?: any,
  duration?: number,
  driving?:string,
  status?: string
}
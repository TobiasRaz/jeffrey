export interface Position {
  id?: string,
  placeName?: string,
  latitude: number,
  longitude: number
}
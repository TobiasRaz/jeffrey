export interface Stripe {
    c_number: number,
    c_name: string,
    c_exp_month: number,
    c_exp_year: number,
    c_cvc: number,
}
import { App } from 'ionic-angular';
import { Component } from '@angular/core';
import { Platform, MenuController, Events, LoadingController, Loading } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { NetworkProvider } from '../providers/network/network';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { UberProvider } from '../providers/uber/uber';

import { ProfilePage } from '../pages/profile/profile';
import { ParametresPage } from '../pages/parametres/parametres';
import { PaymentPage } from '../pages/payment/payment';
import { HelpPage } from '../pages/help/help';
import { DriverVehiclesPage } from '../pages/driver-vehicles/driver-vehicles';
import { MyCoursePage } from '../pages/my-course/my-course';
import { MapPage } from '../pages/map/map';
import { OptionPage } from '../pages/option/option';
import { VotePage } from '../pages/vote/vote';

import { User } from '../models/user';
import { Race } from '../models/race';
import { Profile } from '../models/profile';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  profile = {} as Profile;
  user = {} as User;
  profile_p:any = ProfilePage;
  param_p:any = ParametresPage;
  payement_p:any =PaymentPage;
  help_p:any = HelpPage;
  driver_p:any = DriverVehiclesPage;
  course_p: any = MyCoursePage;
  option_p: any = OptionPage;
  vote_p: any = VotePage;
  isDriver: boolean = false;
  profile_set: boolean = false;
  loading: Loading;
  registered: boolean = false;
  runing: boolean = false;
  constructor(
    private app : App, platform: Platform, statusBar: StatusBar, 
    public menu : MenuController, private fire: FirebaseProvider,
    public splashScreen: SplashScreen, network: NetworkProvider,
    private loadingCtrl: LoadingController, private events: Events,
    private uber: UberProvider
    ) {

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.presentLoading();
    network.checkConnection();
    this.fire.loggedinChange().subscribe((user: any) => {
      if (user && !this.registered) {
        this.user = user;
        this.setProfile().then(() => {
          this.loading.dismiss()
          this.getUnpaid()
        })
      } else {
        this.fire.logout().then(() => {
          this.registered = false;
        })
        this.loading.dismiss();
      }
    })

    this.events.subscribe('select:user', (user) => {
      this.user = user;
      this.setProfile();
    })

    this.events.subscribe('user:logout', () => this.logout())

    this.events.subscribe('user:registered', (register) => this.registered = register)
  }

  logout() {
    this.user = {} as User;
    this.profile = {} as Profile;
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: false,
      enableBackdropDismiss: false
    });
    this.loading.present();
  }

  setProfile() {
    return new Promise((resolve, reject) => {
      this.fire.getProfileChange(this.user.uid)
      .subscribe((profile) => {
        if (profile) {
          for (var key in profile) {
            this.profile = profile[key]
            this.profile.item = key
            resolve(true);
          }
        } else
          reject();
      })
    })
  }

  getRuning(client = this.user) {
    this.fire.getRunning(client)
    .subscribe((runing) => {
        if (runing && Object.keys(runing).length && !this.runing) {
          this.runing = true;
          let race: any;
          let uid = Object.keys(runing)[0];
          let key = Object.keys(runing[uid])[0];
          race = runing[uid][key];
          race.driver = uid;
          race.key = key;
          if (uid == this.user.uid)
            this.isDriver = true
          this.setRoot(MapPage, { run: race, runing: true, isDriver: this.isDriver })
        } else {
          if (!runing && !this.runing) {
            this.setRoot(OptionPage, { user: this.user, profile: this.profile })
          }
        }
    });
  }

  getUnpaid() {
    this.fire.getArchive(this.user.uid).subscribe((unpaid: [Race]) => {
      console.log(unpaid)
      if (unpaid && unpaid[0]) {
        let run = unpaid[0];
        let est_price: any;
        let price: number;
        this.uber.getEstimation(unpaid[0]).then((prices) => {
          switch (run.type) {
            case "VTC":
              est_price = prices[1]
              break;
            case "VAN":
              est_price = prices[4]
              break;
          }
          if (est_price.low_estimate && est_price.high_estimate) {
            price = (Number(est_price.low_estimate) + Number(est_price.high_estimate)) / 2
            /**/this.setRoot(MapPage, { user: this.user, price: price, run: run })/**/
          }
        })
      } else {
        console.log('getDriving')
        this.getDriving()
      }
    })
  }

  getDriving() {
    this.fire.getDriving(this.user)
    .subscribe((driving) => {
      if (driving && Object.keys(driving).length) {  
          let key = Object.keys(driving)[0];
          let user = { uid: key } as User;
          this.getRuning(user);
      } else {
        console.log('getRuning')
        this.getRuning();
      }
    })
  }

  setRootPage(page: any) {
    if (page == this.driver_p)
      this.isDriver = true;
    else
      this.isDriver = false;
    let param = { profile: this.profile, user:  this.user };
    this.menu.toggle().then(() => this.setRoot(page, param))
  }

  setRoot(page: any, params: any = null) {
    this.app.getActiveNav().setRoot(page, params);
  }

  public openPage(page = null){
    if (page) {
      this.app.getActiveNav().push(page, { profile: this.profile, user:  this.user })
      .then(() => this.menu.toggle())
    }
  }

  order() {
    let race = {
      origin_name: 'Votre postion',
      destination_name: '',
      
    }
    this.app.getActiveNav().push(MapPage, { race: race });
    this.menu.toggle();
  }
}


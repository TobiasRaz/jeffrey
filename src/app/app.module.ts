import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Network } from '@ionic-native/network';
import { Diagnostic } from '@ionic-native/diagnostic';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicStorageModule } from '@ionic/storage';
import { Keyboard } from '@ionic-native/keyboard';
import { Stripe } from '@ionic-native/stripe';
import { Calendar } from '@ionic-native/calendar';
import { CallNumber } from '@ionic-native/call-number';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ForgoutPage } from '../pages/forgout/forgout';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { FIREBASE_CONFIG } from './app.firebase.config';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { NetworkProvider } from '../providers/network/network';
import { MapPage } from '../pages/map/map';
import { ParametresPage } from '../pages/parametres/parametres';
import { PaymentPage } from '../pages/payment/payment';
import { HelpPage } from '../pages/help/help';
import { DriverVehiclesPage } from '../pages/driver-vehicles/driver-vehicles';
import { ProfilePage } from '../pages/profile/profile';
import { MyCoursePage } from '../pages/my-course/my-course';
import { ErrorPage } from '../pages/error/error';
import { StripePage } from '../pages/stripe/stripe' ;
import { AddVehiclePage } from '../pages/add-vehicle/add-vehicle';
import { WaitingListPage } from '../pages/waiting-list/waiting-list';
import { YourRacePage } from '../pages/your-race/your-race';
import { EventPage } from '../pages/event/event';
import { ProfilBusinessPage } from '../pages/profil-business/profil-business';
import { AdressProPage } from '../pages/adress-pro/adress-pro';
import { HomeStatePage } from '../pages/home-state/home-state';
import { OfficeParamPage } from '../pages/office-param/office-param';
import { OtherPlacePage } from '../pages/other-place/other-place';
import { ConfParamPage } from '../pages/conf-param/conf-param';
import { OptionPage } from '../pages/option/option';
import { RacingDetailPage } from '../pages/racing-detail/racing-detail';
import { VotePage } from '../pages/vote/vote';
import { DestinationBlocComponent } from '../components/destination-bloc/destination-bloc';
import { DetailChauffeurBlocComponent } from '../components/detail-chauffeur-bloc/detail-chauffeur-bloc';
import { DetailCommandeComponent } from '../components/detail-commande/detail-commande';
import { LoadingComponent } from '../components/loading/loading';
import { DisponibilityComponent } from '../components/disponibility/disponibility';
import { CallingDriverComponent } from '../components/calling-driver/calling-driver';
import { ClientDetailBlocComponent } from '../components/client-detail-bloc/client-detail-bloc';
import { AutoCompleteGoogleInputComponent } from '../components/auto-complete-google-input/auto-complete-google-input';

import { StripeProvider } from '../providers/stripe/stripe';
import { SuperProvider } from '../providers/super/super';
import { UberProvider } from '../providers/uber/uber';
import { WsProvider } from '../providers/ws/ws';
import { NgxStripeModule } from 'ngx-stripe';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    ForgoutPage,
    MapPage,
    ParametresPage,
    PaymentPage,
    HelpPage,
    DriverVehiclesPage,
    ProfilePage,
    MyCoursePage,
    ErrorPage,
    StripePage,
    AddVehiclePage,
    WaitingListPage,
    YourRacePage,
    EventPage,
    ProfilBusinessPage,
    AdressProPage,
    HomeStatePage,
    OfficeParamPage,
    OtherPlacePage,
    ConfParamPage,
    DestinationBlocComponent,
    DetailChauffeurBlocComponent,
    DetailCommandeComponent,
    LoadingComponent,
    DisponibilityComponent,
    CallingDriverComponent,
    OptionPage,
    VotePage,
    ClientDetailBlocComponent,
    AutoCompleteGoogleInputComponent,
    RacingDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    IonicStorageModule.forRoot(),
    HttpModule,
    NgxStripeModule.forRoot('pk_test_ymJmF756QPBJwiTuR0iPel7k'),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    ForgoutPage,
    MapPage,
    ParametresPage,
    PaymentPage,
    HelpPage,
    DriverVehiclesPage,
    ProfilePage,
    MyCoursePage,
    ErrorPage,
    StripePage,
    AddVehiclePage,
    WaitingListPage,
    YourRacePage,
    EventPage,
    AdressProPage,
    HomeStatePage,
    OfficeParamPage,
    OtherPlacePage,
    ProfilBusinessPage,
    ConfParamPage,
    DestinationBlocComponent,
    DetailChauffeurBlocComponent,
    DetailCommandeComponent,
    LoadingComponent,
    DisponibilityComponent,
    CallingDriverComponent,
    OptionPage,
    VotePage,
    ClientDetailBlocComponent,
    AutoCompleteGoogleInputComponent,
    RacingDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseProvider,
    NetworkProvider,
    Network,
    Diagnostic,
    GoogleMaps,
    Camera,
    Geolocation,
    Keyboard,
    StripeProvider,
    Stripe,
    SuperProvider,
    Calendar,
    CallNumber,
    UberProvider,
    WsProvider
  ]
})
export class AppModule {}
